<?php

namespace LakeDrops\Ahoy;

use LakeDrops\Component\Composer\BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Composer Update Command for Ahoy.
 *
 * @package LakeDrops\Ahoy
 */
class UpdateCommand extends BaseCommand {

  /**
   * {@inheritdoc}
   */
  protected function configure(): void {
    parent::configure();
    $this
      ->setName('lakedrops:ahoy')
      ->setDescription('Scan LakeDrops plugins for ahoy scripts.');
  }

  /**
   * {@inheritdoc}
   */
  public function getHandlerClass(): string {
    return Handler::class;
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output): int {
    parent::execute($input, $output);
    /** @var Handler $handler */
    $handler = $this->handler;
    $handler->updateScripts();
    return 0;
  }

}
