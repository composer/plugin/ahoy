<?php

namespace LakeDrops\Ahoy;

use LakeDrops\Component\Composer\BaseHandler;
use Symfony\Component\Yaml\Yaml;

/**
 * Handler class to setup Drupal projects for Behat tests.
 *
 * @package LakeDrops\Ahoy
 */
class Handler extends BaseHandler {

  /**
   * {@inheritdoc}
   */
  public function configId(): string {
    return 'ahoy';
  }

  /**
   * Update ahoy scripts of all LakeDrops plugins.
   */
  public function updateScripts(): void {
    $this->init();

    // We only do the fancy stuff for developers.
    if (!$this->isDevMode()) {
      return;
    }

    if (file_exists('.ahoy.yml')) {
      $ahoy = Yaml::parseFile('.ahoy.yml');
    }
    else {
      $ahoy = [
        'ahoyapi' => 'v2',
        'commands' => [],
      ];
    }

    $rootDir = getcwd();
    $installationManager = $this->composer->getInstallationManager();
    foreach ($this->composer->getRepositoryManager()->getLocalRepository()->getPackages() as $pkg) {
      $pluginRoot = $installationManager->getInstallPath($pkg);
      if (file_exists($pluginRoot . '/.ahoy.l3d.yml')) {
        $pluginAhoy = Yaml::parseFile($pluginRoot . '/.ahoy.l3d.yml');
        foreach ($pluginAhoy['commands'] as $command => $commands) {
          $ahoy['commands'][$command] = $commands;
          if (isset($commands['imports']) && is_array($commands['imports'])) {
            foreach ($commands['imports'] as $key => $import) {
              $ahoy['commands'][$command]['imports'][$key] = '.' . substr($pluginRoot, strlen($rootDir)) . '/' . $import;
            }
          }
        }
      }
    }

    // Cleanup configuration: remove non-existant includes.
    foreach ($ahoy['commands'] as $command => $commands) {
      if (isset($commands['imports']) && is_array($commands['imports'])) {
        $deleteKeys = [];
        foreach ($commands['imports'] as $key => $import) {
          if (!file_exists($import)) {
            $deleteKeys[] = $key;
          }
        }
        arsort($deleteKeys);
        foreach ($deleteKeys as $deleteKey) {
          unset($ahoy['commands'][$command]['imports'][$deleteKey]);
        }
        if (empty($ahoy['commands'][$command]['imports'])) {
          unset($ahoy['commands'][$command]);
        }
      }
    }

    $file = $rootDir . '/.ahoy.yml';
    file_put_contents($file, Yaml::dump($ahoy, 9, 2));
    $this->gitIgnore('.ahoy.yml');
  }

}
