<?php

namespace LakeDrops\Ahoy;

use Composer\Plugin\Capability\CommandProvider as ComposerCommandProvider;
use Composer\Script\Event;
use Composer\Script\ScriptEvents;
use LakeDrops\Component\Composer\BasePlugin;

/**
 * Composer plugin for handling Ahoy scripts.
 */
class Plugin extends BasePlugin {

  /**
   * {@inheritdoc}
   */
  public function getHandlerClass(): string {
    return Handler::class;
  }

  /**
   * {@inheritdoc}
   */
  public function getCapabilities(): array {
    return [
      ComposerCommandProvider::class => CommandProvider::class,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      ScriptEvents::POST_CREATE_PROJECT_CMD => 'updateScripts',
      ScriptEvents::POST_INSTALL_CMD => 'updateScripts',
      ScriptEvents::POST_UPDATE_CMD => 'updateScripts',
    ];
  }

  /**
   * Update ahoy scripts of all LakeDrops plugins.
   *
   * @param \Composer\Script\Event $event
   *   The event that triggered the plugin.
   */
  public function updateScripts(Event $event): void {
    /** @var \LakeDrops\Ahoy\Handler $handler */
    $handler = $this->handler;
    $handler->setEvent($event);
    $handler->updateScripts();
  }

}
