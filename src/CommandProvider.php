<?php

namespace LakeDrops\Ahoy;

use Composer\Plugin\Capability\CommandProvider as CommandProviderCapability;

/**
 * Composer Command Provider for Ahoy.
 *
 * @package LakeDrops\Ahoy
 */
class CommandProvider implements CommandProviderCapability {

  /**
   * {@inheritdoc}
   */
  public function getCommands(): array {
    return [
      new UpdateCommand(),
    ];
  }

}
